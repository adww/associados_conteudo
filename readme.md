# Arquivos multimídia

Descrição completa do material multimídia utilizando para alimentar as redes sociais: Youtube e Soundcloud.

## Áudio
Esses quatro primeiros áudios foram gravados pela **Rádio UFOP Educativa, 106.3FM** no dia 03/12/15

1. Áudio-descrição: quebrando barreiras
2. Áudio-descrição: o que é?
3. Áudio-descrição: a importância
4. Áudio-descrição: destinação